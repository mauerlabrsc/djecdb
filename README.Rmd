---
title: "README"
author: "Lina Marcela Gallego-Paez"
date: "05/10/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## DJEC DB

This is a light version of DJEC DB, a custom database of differential junction expression in cancer. DJEC DB _light_ contains data analysis on 5 tumor cohorts from TCGA (BRCA, COADREAD, LUAD, LUSC and LGG) and the correspondent tissue types in CCLE (Breast, Colorectal, Lung and Brain). The data can be interactively visualized using a shiny-based graphical interface.

For the user to open the graphical interphace, the entire content of the database has to be copied locally and the app.R file opened and run (using for instance, RStudio).

The R package dependencies required for the graphical interface to display correctly are the following:

- shiny
- shinythemes
- shinyCyJS
- shinyalert
- shinycssloaders
- DT
- ggplot2
- highcharter
- tibble
- limma
- edgeR
- stringr
- grid
- dplyr
- Cairo
- xts
- visNetwork
- geomnet
- igraph
- rCharts
- heatmaply
- WGCNA
- RColorBrewer
- radarchart
- webshot


